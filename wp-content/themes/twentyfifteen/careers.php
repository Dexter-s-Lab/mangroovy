
<?php

/*
 Template Name: Careers
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/careers.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;

$main_bg = get_field('main_bg', get_the_ID());
$main_title_1 = get_field('main_title_1', get_the_ID());
$main_title_2 = get_field('main_title_2', get_the_ID());

$array_menu = wp_get_nav_menu_items("main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}

$args = array( 'numberposts' =>-1, 'post_type'=>'vacancies', 'suppress_filters' => 0, 'order' => 'ASC');
$careers = get_posts( $args );
$careers_items = array();


foreach ( $careers as $post ) :   setup_postdata( $post );

$title = $post->post_title;
$desc = get_field('desc', get_the_ID());
$ref_code = get_field('ref_code', get_the_ID());


$careers_items[] = array('title' => $title, 'desc' => $desc, 'ref_code' => $ref_code);

endforeach;
wp_reset_postdata();

// print_r($careers_items);die;

?>


<div class="main_div container">

  <?php 
  include 'nav_bar.php';
  ?>
  <div class="top_div" style="background-image:url(<?= $main_bg; ?>)">
    <div class="top_div_inner">
      <span class="top_span_1"><?= $main_title_1; ?></span>
      <span class="top_span_2"><?= $main_title_2; ?></span>
    </div>
  </div>
</div>
<div class="loc_div">
  <div class="locs loc_1">
    <div class="loc_div_inner">
      <img class="idle_imgs loc_1_btn" src="assets/img/submit_btn.png">
      <!-- <img class="active_imgs" src="assets/img/submit_btn_hover.png"> -->
      <span class="loc_span_1">Submit a CV</span>
      <span class="loc_span_2">Interested in a job with us?</span>
      <span class="loc_span_3">Submit your CV now</span>
    </div>
    <div class="loc_div_inner">
      <img class="idle_imgs loc_2_btn" src="assets/img/browse_btn.png">
      <!-- <img class="active_imgs" src="assets/img/browse_btn_hover.png"> -->
      <span class="loc_span_1">Browse Jobs</span>
      <span class="loc_span_2">Find the perfect opening</span>
      <span class="loc_span_3">and apply with a click</span>
    </div>
  </div>
  <div class="locs loc_2">
    <img class="back_btn" src="assets/img/back_btn.png">
    <form class="submit_cv_form" formID="2">
      
    <!-- <form method="post" enctype="multipart/form-data" target="gform_ajax_frame_2" id="gform_2" action="/Hug_Digital/Mangroovy/Application/careers/#gf_2"> -->
      <span class="loc_2_spans loc_2_span_1">Submit your CV</span>
      <span class="loc_2_spans loc_2_span_2">Attach your resume and send it now.</span>
      <input class="form_inputs" inputID="1"  name="input_1" id="input_2_1" type="file" accept="application/pdf,application/msword,
    application/vnd.openxmlformats-officedocument.wordprocessingml.document">
      <span class="loc_2_spans loc_2_span_3">Documents allowed (pdf.,doc.,docx.)</span>
      <button type="submit" class="btn btn-default submit_btn">Submit</button>
    </form>
  </div>
  <div class="locs loc_3">
    <img class="back_btn back_btn_2" src="assets/img/back_btn.png">
    <span class="loc_3_span_1">Available Vacancies</span>

    <?php
    foreach ($careers_items as $key => $value) {
      ?>
      <div class="career_item">
        <form class="submit_career_form" formID="3">
          <span class="career_title"><?= $value['title']; ?></span>
          <span class="career_desc_title">Job Description:</span>
          <p class="career_desc"><?= $value['desc']; ?></p>
          <button type="button" class="apply_btn app_new" apID="<?= $key;?>">Apply Now</button>
          <div class="subs sub_<?= $key;?>">
            <input inputID="1"  name="input_1" id="input_3_1" type="hidden" value="<?= $value['title']; ?>">
            <input class="form_inputs" inputID="2"  name="input_2" id="input_3_2" type="file" accept="application/pdf,application/msword,
            application/vnd.openxmlformats-officedocument.wordprocessingml.document">
              <span class="loc_2_spans loc_2_span_3">Documents allowed (pdf.,doc.,docx.)</span>
              <button type="submit" class="btn btn-default submit_btn">Submit</button>
          </div>
        </form>
      </div>
      <?php
    }
    ?>
  </div>
</div>
<div class="container fac_div">
  
</div>

<?php 
  include 'footer_div.php';
  ?>

<?php get_footer(); ?>

<script>

$( document ).ready(function() {


  
$( ".loc_1_btn" ).on( "click", function(event) {
  
  $('.locs').hide();
  $('.loc_2').fadeIn();

});


$( ".loc_2_btn" ).on( "click", function(event) {
  
  $('.locs').hide();
  $('.loc_3').fadeIn();

});

$( ".back_btn" ).on( "click", function(event) {
  
  $('.locs').hide();
  $('.loc_1').fadeIn();

});

$( ".app_new" ).on( "click", function(event) {
  
  var apID = $(this).attr('apID');
  $(this).hide();
  $('.sub_' + apID).fadeIn();
  // $('.locs').hide();
  // $('.loc_1').fadeIn();

});

$( ".submit_cv_form" ).submit(function( event ) {
    
    event.preventDefault();
    return false;

    var formID = $(this).attr('formID');
    var x = $('.submit_cv_form input').clone();
    
    $('.ginput_container input').remove();
    $('.ginput_container').append(x);
    
    $('#loading_2').show();

    $('#gform_' + formID).submit();
    
    
    jQuery(document).bind('gform_post_render', function(){

      $('#loading_2').fadeOut();
      return false;

    });

  });

$( ".submit_career_form" ).submit(function( event ) {
    
    event.preventDefault();
    return false;

    var formID = $(this).attr('formID');
    return false;
    var x = $('.submit_career_form input').clone();
    
    $('.ginput_container input').remove();
    $('.ginput_container').append(x);
    
    $('#loading_2').show();

    $('#gform_' + formID).submit();
    
    
    jQuery(document).bind('gform_post_render', function(){

      $('#loading_2').fadeOut();
      return false;

    });

  });

  });


window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';

 var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;


$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);

$('#loading ').fadeOut();

}

$(window).on('resize', function()
{

var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;

$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>