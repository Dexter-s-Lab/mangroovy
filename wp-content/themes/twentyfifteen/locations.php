
<?php

/*
 Template Name: Locations
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/locations.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">
<input value="<?php echo site_url(); ?>/wp-content/themes/twentyfifteen/assets/img/test.png" type="hidden" class="marker_url">

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 
?>

<script type="text/javascript">

var directionsService;
var directionsDisplay;
var map1;
var flightPath;
var bounds;
var position_1;
var marker;

function initMap() 
{

    directionsService = new google.maps.DirectionsService;
    directionsDisplay = new google.maps.DirectionsRenderer;

    var mapCanvas1 = document.getElementById('map1');
    var mapOptions1 = {
      center: new google.maps.LatLng(27.4162992, 33.6716665),
      // center: new google.maps.LatLng(31.691361, 30.101870),
      zoom: 12,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    map1 = new google.maps.Map(mapCanvas1, mapOptions1);
    position_1 = {lat: 27.4162992, lng: 33.6716665};



    var marker_1 = new google.maps.Marker({
      position: position_1,
      map: map1,
      title: 'Mangroovy Residence'
      // icon: $('.marker_url').val()
    });

    // bounds.extend(position_2);

    // map1.fitBounds(bounds);


}

function calculateAndDisplayRoute(directionsService, directionsDisplay, lat, lng) {

if(flightPath)
flightPath.setMap(null);

if(marker)
marker.setMap(null);

var flightPlanCoordinates = [
    {lat: lat, lng: lng},
    {lat: 27.4162992, lng: 33.6716665},
  ];
  flightPath = new google.maps.Polyline({
    path: flightPlanCoordinates,
    geodesic: true,
    strokeColor: '#FF0000',
    strokeOpacity: 1.0,
    strokeWeight: 3
  });

  var position = {lat: lat, lng: lng};
  marker = new google.maps.Marker({
      position: position,
      map: map1
    });

    bounds = new google.maps.LatLngBounds();
    // bounds.extend(position_1);
    bounds.extend(position_1);
    bounds.extend(position);

    map1.fitBounds(bounds);

  flightPath.setMap(map1);


}
</script>

<?php
global $post;
$page_id =  $post->ID;

$title = $post->post_title;
$location_title_1 = get_field('location_title_1', get_the_ID());
$location_title_2 = get_field('location_title_2', get_the_ID());
$location_text = get_field('location_text', get_the_ID());
$location_image = get_field('location_image', get_the_ID());
$mid_title = get_field('mid_title', get_the_ID());
$global_title = get_field('global_title', get_the_ID());
$global_text = get_field('global_text', get_the_ID());
$internal_title = get_field('internal_title', get_the_ID());
$internal_text = get_field('internal_text', get_the_ID());
$elgouna_title = get_field('elgouna_title', get_the_ID());
$elgouna_text = get_field('elgouna_text', get_the_ID());

$args = array( 'numberposts' =>-1, 'post_type'=>'l_d_golbal', 'suppress_filters' => 0, 'order' => 'ASC');
$l_d_golbal = get_posts( $args );
$l_d_golbal_items = array();


foreach ( $l_d_golbal as $post ) :   setup_postdata( $post );

$title = $post->post_title;
$desc = $post->post_content;
$lat = get_field('lat', get_the_ID());
$lng = get_field('lng', get_the_ID());


$l_d_golbal_items[] = array('title' => $title, 'desc' => $desc, 'lat' => $lat, 'lng' => $lng);

endforeach;
wp_reset_postdata();


$args = array( 'numberposts' =>-1, 'post_type'=>'l_d_internal', 'suppress_filters' => 0, 'order' => 'ASC');
$l_d_internal = get_posts( $args );
$l_d_internal_items = array();


foreach ( $l_d_internal as $post ) :   setup_postdata( $post );

$title = $post->post_title;
$desc = $post->post_content;
$lat = get_field('lat', get_the_ID());
$lng = get_field('lng', get_the_ID());


$l_d_internal_items[] = array('title' => $title, 'desc' => $desc, 'lat' => $lat, 'lng' => $lng);

endforeach;
wp_reset_postdata();


$args = array( 'numberposts' =>-1, 'post_type'=>'l_d_gouna', 'suppress_filters' => 0, 'order' => 'ASC');
$l_d_gouna = get_posts( $args );
$l_d_gouna_items = array();


foreach ( $l_d_gouna as $post ) :   setup_postdata( $post );

$title = $post->post_title;
$desc = $post->post_content;
$lat = get_field('lat', get_the_ID());
$lng = get_field('lng', get_the_ID());


$l_d_gouna_items[] = array('title' => $title, 'desc' => $desc, 'lat' => $lat, 'lng' => $lng);

endforeach;
wp_reset_postdata();

// print_r($l_d_gouna_items);die;

$array_menu = wp_get_nav_menu_items("main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}

?>


<div class="main_div container">

  <?php 
  include 'nav_bar.php';
  ?>
  <div class="top_div">
    <div class="top_div_1 container">
      <div class="top_div_1_left col-sm-8">
        <span><?= $location_title_1; ?></span>
        <span class="top_div_1_span_2"><?= $location_title_2; ?></span>
      </div>
      <div class="top_div_1_right col-sm-4">
        <img src="<?= $location_image; ?>"/>
      </div>
      <!-- <div class="col-sm-2">
      </div> -->
    </div>
    <div class="top_div_2 container">
      <div id="map1" class="top_div_2_left col-sm-8 maps">
      </div>
      <div class="top_div_2_right col-sm-4">
        <?= $location_text; ?>
      </div>
    </div>
  </div>
  <div class="mid_div">
    <span><?= $mid_title; ?></span>
  </div>
    <div class="locations locations_div container">
      <div class="locations_global locs col-sm-4">
        <div class="loc_divs loc_div_1 container" arrowID="loc_1">
          <div class="loc_divs_in col-sm-12">
            <span class="locations_span_1">
            <?= $global_title; ?>
            </span>
          </div>
          <img class="arrow_down" src="assets/img/arrow_down.png">
          <!-- <div class="col-sm-2">
            <img class="arrow_down" arrowID="loc_1" src="assets/img/arrow_down.png">
          </div> -->
        </div>
        <div class="loc_1">
          <div class="locations_2">
            <?= $global_text; ?>
          </div>
          <div class="loc_list">
            <?php
            foreach ($l_d_golbal_items as $key => $value) {

              $class = "odd_list";

              if($key % 2 == 0)
              $class ="even_list";

              ?>
              <span class="all_lists <?= $class; ?>" lat="<?= $value['lat']; ?>" lng="<?= $value['lng']; ?>"><?= $value['title']; ?>  -  <?= strip_tags($value['desc']); ?></span>
              <?php
            }
            ?>
          </div>
        </div>
      </div>
      <div class="locations_inter locs col-sm-4">
        <div class="loc_divs loc_div_1 container" arrowID="loc_2">
          <div class="loc_divs_in col-sm-12">
            <span class="locations_span_1">
            <?= $internal_title; ?>
            </span>
          </div>
          <img class="arrow_down" src="assets/img/arrow_down.png">
          <!-- <div class="col-sm-2">
            <img class="arrow_down" arrowID="loc_2" src="assets/img/arrow_down.png">
          </div> -->
        </div>
        <div class="loc_2">
          <div class="locations_2">
            <?= $internal_text; ?>
          </div>
          <div class="loc_list">
            <?php
            foreach ($l_d_internal_items as $key => $value) {

              $class = "odd_list";

              if($key % 2 == 0)
              $class ="even_list";

              ?>
              <span class="all_lists <?= $class; ?>" lat="<?= $value['lat']; ?>" lng="<?= $value['lng']; ?>"><?= $value['title']; ?>  -  <?= strip_tags($value['desc']); ?></span>
              <?php
            }
            ?>
          </div>
        </div>
      </div>
      <div class="locations_gouna locs col-sm-4">
        <div class="loc_divs loc_div_1 container" arrowID="loc_3">
          <div class="loc_divs_in col-sm-12">
            <span class="locations_span_1">
            <?= $elgouna_title; ?>
            </span>
          </div>
          <img class="arrow_down" src="assets/img/arrow_down.png">
          <!-- <div class="col-sm-1">
            <img class="arrow_down" arrowID="loc_3" src="assets/img/arrow_down.png">
          </div> -->
        </div>
        <div class="loc_3">
          <div class="locations_2">
            <?= $elgouna_text; ?>
          </div>
          <div class="loc_list">
            <?php
            foreach ($l_d_gouna_items as $key => $value) {

              $class = "odd_list";

              if($key % 2 == 0)
              $class ="even_list";

              ?>
              <span class="all_lists <?= $class; ?>" lat="<?= $value['lat']; ?>" lng="<?= $value['lng']; ?>"><?= $value['title']; ?>  -  <?= strip_tags($value['desc']); ?></span>
              <?php
            }
            ?>
          </div>
        </div>
      </div>
  </div>
</div>

<?php 
  include 'footer_div.php';
  ?>

<?php get_footer(); ?>

<script>

$( document ).ready(function() {


  $('.loc_divs').on('click',function (e) {

    var arrowID = $(this).attr('arrowID');
    $('.'+arrowID).slideToggle();

  });

  $('.all_lists').on('click',function (e) {

    var lat = parseFloat($(this).attr('lat'));
    var lng = parseFloat($(this).attr('lng'));

    calculateAndDisplayRoute(directionsService, directionsDisplay,lat,lng);

    $('html, body').animate({
        scrollTop: $(".top_div").offset().top
    }, 1000);

  });

});

$(window).scroll(function() {



});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';

 var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;


$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);

initMap();

$('#loading ').fadeOut();

}

$(window).on('resize', function()
{

var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;

$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);


});

</script>

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCe8ySOOlQeodi3WENjiXmopN24CvJEsto&libraries=geometry"></script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>