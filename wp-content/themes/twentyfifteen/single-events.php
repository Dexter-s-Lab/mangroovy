
<?php

/*
 Template Name: Events Single
 */

$page_title = $wp_query->post->post_title;

get_header(); 

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/media_single.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">

<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>

<script type="text/javascript" src="assets/classyBox/js/jquery.classybox.js"></script>
<script type="text/javascript" src="assets/classyBox/js/jwplayer.js"></script>
<script type="text/javascript" src="assets/classyBox/js/jwplayer.html5.js"></script>
<link rel="stylesheet" type="text/css" href="assets/classyBox/css/jquery.classybox.css" />



<?php 
//include 'spinner.php'; 
?>

<?php


global $post;
$page_id =  $post->ID;

$title = $post->post_title;
$desc = get_field('desc', $page_id);
$gallery = get_field('gallery', $page_id);
$videos = get_field('videos', $page_id);


$all_videos = array();

if($videos)
{
foreach ( $videos as $video_single ) {

// print_r($video_single);die;
$title = $video_single -> post_title;
$video = $video_single -> post_content;

$id = explode('v=', $video);

if(isset($id[1]))
{
  $id = explode('"', $id[1]);
  $id = explode('&', $id[0]);
  $id = $id[0];

  $thumb = 'http://img.youtube.com/vi/'.$id.'/hqdefault.jpg';
  $video = 'http://www.youtube.com/watch?v='.$id;
}

else
{
  $id = explode('.com/', $video);
  $id = $id[1];

  $image_url = parse_url($video);
    
  $ch = curl_init('http://vimeo.com/api/v2/video/'.substr($image_url['path'], 1).'.php');
  curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt ($ch, CURLOPT_FOLLOWLOCATION, 1);
  $a = curl_exec($ch);
  $hash = unserialize($a);

  $thumb = $hash[0]["thumbnail_large"];
  $video = 'https://player.vimeo.com/video/'.$id;

}


$all_videos[] = array('title'=> $title, 'video'=> $video, 'thumb'=> $thumb);

}
}
wp_reset_postdata();


$all_images = array();
$gallery = $gallery[0]['ngg_id'];
global $nggdb;
$images = $nggdb -> get_gallery($gallery);
// print_r($images);die;
foreach ( $images as $image ) {
 $all_images[] = $image->imageURL;
}


// print_r($all_images);die;

$array_menu = wp_get_nav_menu_items("main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}


// print_r($events_items);die;
?>

<div class="main_div container">

  <?php 
  include 'nav_bar.php';
  ?>
  <div class="top_div">
    <span class="top_div_span">
      <?= $title ?>
    </span>
    <div class="top_div_desc">
      <?= $desc; ?>
    </div>
  </div>
</div>
<div class="loc_div container">
  <div class="col-sm-12 loc_left_div">
    <span class="loc_span_1"><?= $main_title; ?></span>
    <p class="main_text"><?= $main_text; ?></p>
  </div>
</div>
<div class="container fac_div">
  <a href="<?= get_permalink(83);?>">
    <img class="back_btn" src="assets/img/back_btn.png">
  </a>
  <ul id="filters" class="clearfix">
        <?php

        $active_img = '';
        $active_vid = '';

        if(!empty($all_images))
        {
          $active_img = 'active_div';

          ?>
          <li class="active"><span class="filter photos_filter_2" fID="photos">Photos</span></li>
          <?php
        }
       if(!empty($all_videos))
        {
          if(empty($all_images))
          $active_vid = 'active_div';
        
          {
            ?>
            <li class="active"><span class="filter video_filter" fID="videos">Videos</span></li>
            <?php
          }
        }
        ?>
      </ul>

      <div id="portfoliolist" class="container photos_filter div_filters <?= $active_img; ?>">
        <ul id="gallery-container">

            <?php
            foreach ($all_images as $key => $value) {

              list($width, $height) = getimagesize($value);
              $flag ='';
              $image_class = 'landscape';
              if($height > $width)
              {
                $image_class = 'portrait';
                // $flag = 'portrait_div';
              }
              

              ?>
              <a class="fancybox thumbnail <?= $flag ?>" rel="group" href="<?= $value ?>" style="background-image: url(<?= $value ?>)">
              </a>
              <?php
            }
            ?>

          </ul>
      </div>

      <div id="portfoliolist" class="container videos_filter div_filters <?= $active_vid; ?>">
        <div class="gallery_row container gallery">
        <?php

        foreach ($all_videos as $key => $value) {

          ?>
          <p>
          <a href="<?= $value['video'] ?>" class="html5lightbox" data-nativehtml5controls="true" data-group="myvideo" data-thumbnail="https://img.youtube.com/vi/_mMKNw_Mlxs/default.jpg">
            <div class="gallery_cols col-sm-4">
              <img src="<?= $value['thumb'] ?>"/>
              <div class="gallery_title">
                <span>
                  <?= $value['title'] ?>
                </span>
              </div>
              
            </div>
          </a>
          <?php
        }
        ?>
        </div>
      </div>
</div>

<?php 
  include 'footer_div.php';
  ?>

<?php get_footer(); ?>


<link rel="stylesheet" href="assets/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
<script type="text/javascript" src="assets/fancybox/jquery.fancybox.pack.js"></script>
<script type="text/javascript" src="assets/js/html5lightbox/html5lightbox.js"></script>



<script>

$( document ).ready(function() {


 $('.filter').on('click',function (e) {
      
      var fID = $(this).attr('fID');

      $('.active_div').removeClass('active_div');
      $('.'+fID+'_filter').addClass('active_div');

      $('#filters .active').removeClass('active');
      $('.'+fID+'_filter_2').parent().addClass('active');
});


$( ".back_btn" ).on( "click", function(event) {
  
  $('.locs').hide();
  $('.loc_1').fadeIn();

});

});



window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';

 var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;


$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);

$(".fancybox").fancybox();


$('.custom_Events').addClass('active');
$('#loading ').fadeOut();

}

$(window).on('resize', function()
{

var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;

$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>