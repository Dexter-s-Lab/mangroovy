
<?php

/*
 Template Name: Units & Architecture
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/units.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">
<link href="assets/css/hover.css" rel="stylesheet" media="screen">

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;


$main_bg = get_field('main_bg', get_the_ID());
$main_title_1 = get_field('main_title_1', get_the_ID());
$main_title_2 = get_field('main_title_2', get_the_ID());
$main_text = get_field('main_text', get_the_ID());
$current_slider = $post ->post_name;

$array_menu = wp_get_nav_menu_items("main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
        $menu[$counter]['slug']         =   $m->post_name;
    }

    $counter++;
}


global $post;
$page_id =  $post->ID;

// print_r($cat_array);die;
$args = array( 'numberposts' =>-1, 'post_type'=>'units_fac', 'suppress_filters' => 0, 'order' => 'ASC');
$units = get_posts( $args );
$unit_items = array();

$current_unit = '';

foreach ( $units as $post ) :   setup_postdata( $post );

$title = $post->post_title;
// $desc = $post->post_content;
$desc = get_field('desc', get_the_ID());
$slider = get_field('slider', get_the_ID());
$page_link = get_field('page', get_the_ID());
$current = '';

if($slider == $current_slider)
{
  $current_unit = array('title' => $title, 'desc' => $desc, 'slider' => $slider, 'page' => $page_link);
  $current = 'mid_div_active';
}



// print_r($post);die;

$unit_items[] = array('title' => $title, 'page' => $page_link, 'current' => $current);



endforeach;
wp_reset_postdata();

// print_r($unit_items);die;
// print_r($current_unit);die;

?>


<?php 
//include 'analyticstracking.php' 
?>
<?php 
//include 'mobile_nav.php' 
?>

<div class="main_div container">

  <?php 
  include 'nav_bar.php';
  ?>
  <!-- <div class="nav_bar">
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="logo_nav"><a href="<?php echo get_site_url(); ?>"><img class="event_logo" src="assets/img/man_logo_header.png"></a></li>
        <li class="menu_items <?= $class;?>">
          <a href="#home">Home</a>
        </li>
      </ul>
    </div>
  </div> -->
  <div class="top_div" style="background-image:url(<?= $main_bg; ?>)">
    <div class="top_text_div">
      <div class="top_text_div_inner">
        <span class="top_span_1"><?= $main_title_1; ?></span>
        <span class="top_span_2"><?= $main_title_2; ?></span>
        <p class="top_p"><?= $main_text; ?></p>
      </div>
    </div>
  </div>
  <div class="top_text_div_mobile">
    <span class="top_span_1"><?= $main_title_1; ?></span>
    <span class="top_span_2"><?= $main_title_2; ?></span>
    <div class="extra_border"></div>
    <p class="top_p"><?= $main_text; ?></p>
  </div>
  <div class="mid_div">
    <?php
    foreach ($unit_items as $key => $value) {

      end($unit_items);
      $lastKey = key($unit_items);
      $class = "";
      $class_2 = "";

      if($key == $lastKey)
      $class = "mid_div_last";


      ?>
      <a href="<?= $value['page']; ?>" class="mid_div_href">
        <span class="<?= $value['current']; ?> <?= $class_2; ?>"><?= $value['title']; ?></span>
      </a>
      <?php
    }
    ?>
  </div>
  <div class="bot_div">
    <div class="bot_div_inners">
        <div class="bot_div_inner">
          <span class="bot_div_title"><?= $current_unit['title']; ?></span>
          <!-- <p class="bot_div_desc"><?= $value['desc']; ?></p> -->
          <?= $current_unit['desc']; ?>
        </div>
      </div>
      <div class="all_sliders">
        <?php
        echo do_shortcode('[rev_slider alias="'.$current_unit['slider'].'"]');
        ?>
      </div>
  </div>
</div>

<?php 
  include 'footer_div.php';
  ?>

<?php get_footer(); ?>


<script>

$( document ).ready(function() {


  $('.mid_div_href').on('click',function (e) {



  });

  });

$(window).scroll(function() {




});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';

 var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;


$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);

$('.custom_units-architecture').addClass('active');

$('#loading ').fadeOut();


}

$(window).on('resize', function()
{

var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;

$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>