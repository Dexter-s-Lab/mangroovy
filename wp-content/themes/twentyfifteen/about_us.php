
<?php

/*
 Template Name: About Us
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/about_us.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;

$main_bg = get_field('main_bg', get_the_ID());
$image_title_1 = get_field('image_title_1', get_the_ID());
$image_title_2 = get_field('image_title_2', get_the_ID());
$image_text = get_field('image_text', get_the_ID());


$desc_title_1 = get_field('desc_title_1', get_the_ID());
$desc_title_2 = get_field('desc_title_2', get_the_ID());
$desc_text = get_field('desc_text', get_the_ID());

$array_menu = wp_get_nav_menu_items("main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}
// print_r($menu);die;

?>


<?php 
//include 'analyticstracking.php' 
?>
<?php 
//include 'mobile_nav.php' 
?>

<div class="main_div container">

  <?php 
  include 'nav_bar.php';
  ?>
  <!-- <div class="nav_bar">
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="logo_nav"><a href="<?php echo get_site_url(); ?>"><img class="event_logo" src="assets/img/man_logo_header.png"></a></li>
        <li class="menu_items <?= $class;?>">
          <a href="#home">Home</a>
        </li>
      </ul>
    </div>
  </div> -->
  <div class="top_div" style="background-image:url(<?= $main_bg; ?>)">
    <div class="top_text_div">
      <div class="top_text_div_inner">
        <span class="top_span_1"><?= $image_title_1; ?></span>
        <span class="top_span_2"><?= $image_title_2; ?></span>
        <p class="top_p"><?= $image_text; ?></p>
      </div>
    </div>
  </div>
  <div class="top_text_div_mobile">
    <span class="top_span_1"><?= $image_title_1; ?></span>
    <span class="top_span_2"><?= $image_title_2; ?></span>
    <div class="extra_border"></div>
    <p class="top_p"><?= $image_text; ?></p>
  </div>
</div>
<div class="loc_div container">
  <div class="col-sm-9 loc_left_div">
    <span class="loc_span_1"><?= $desc_title_1; ?></span>
    <span class="loc_span_2"><?= $desc_title_2; ?></span>
    <p class="main_text"><?= $desc_text; ?></p>
  </div>
</div>

<?php 
  include 'footer_div.php';
  ?>

<?php get_footer(); ?>


<script>

$( document ).ready(function() {


  $('.bot_nav_right span').on('click',function (e) {


  });

  });

$(window).scroll(function() {

//   var scroll = $(window).scrollTop();
//   var slide_height = parseInt($('.slider_div').css('height'));

//   if(scroll > 40)
//   {

//     $('.arrow-up-right').css('border-top-width', '0');
//     $('.arrow-up-left').css('border-top-width', '0');


//     $('.nav_bar').addClass('nav_bar_slide');

//   }

//   else
//   {

//     $('.arrow-up-right').css('border-top-width', '90px');
//     $('.arrow-up-left').css('border-top-width', '90px');

//     $('.nav_bar').removeClass('nav_bar_slide');
//   } 
// });


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';

 var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;


$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);

$('#loading ').fadeOut();

}

$(window).on('resize', function()
{

var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;

$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>