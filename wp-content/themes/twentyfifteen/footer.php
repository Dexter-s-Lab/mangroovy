

<script type="text/javascript">

$( document ).ready(function() {


$( ".reg_form" ).submit(function( event ) {
    
    event.preventDefault();
    return false;

    $('#loading_2').show();

    var formID = $(this).attr('formID');
    var inputs = $(this).find('.form_inputs');

    $.each( inputs, function( key, value ) {
      
      inputID = $(value).attr('inputID');
      var input_old = $(value).val();
      $('#input_' + formID + '_' + inputID).val(input_old);


    });

    $('#gform_' + formID).submit();
    
    jQuery(document).bind('gform_post_render', function(){

      $('#loading_2').fadeOut();
      // return false;

      var url = $('.thank_you_url').val();
      window.location.href = url;

      $.each( inputs, function( key, value ) {
        
        $(value).val('');

      });

    });

  });


$( ".dead_links" ).on( "click", function(event) {
  
  event.preventDefault();
  
});


$( ".mid_div_href" ).on( "click", function(event) {
  
  $('.mid_div_href span').removeClass('mid_div_active');
  $(this).find('span').addClass('mid_div_active');
  
});

});

$(window).on('resize', function()
{



});
</script>