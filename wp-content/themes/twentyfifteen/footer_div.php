<div class="footer container">
  <div class="col-sm-3 footer_divs footer_div_1 footer_div_desktop">
    <div class="footer_div_1_inner">
      <img src="assets/img/man_logo_footer.png">
      <span>© <?= date('Y'); ?> Mangroovy Residence</span>
      <span>All rights reserved</span>
    </div>
  </div>
  <div class="col-sm-3 footer_divs footer_div_1 footer_div_mobile_1">
    <div class="footer_div_1_inner">
      <img src="assets/img/man_logo_footer.png">
    </div>
  </div>
  <div class="col-sm-3 footer_divs footer_div_2">
    <?php 

    foreach ($menu as $key => $value) {

    // print_r($value['title']);die;

      ?>
      <a href="<?= $value['url'];?>">
        <span><?= $value['title'];?></span>
      </a>
      <?php
    }
    ?>
  </div>
  <div class="col-sm-3 footer_divs footer_div_3">
    <div class="footer_div_3_inner">
        <div class="footer_div_3_top">
          <a href="<?= $file; ?>" download>
            <span class="footer_div_3_spans_1">Download Brochure</span>
          </a>
          <a href="<?= get_permalink(21); ?>">
            <span class="footer_div_3_spans_1 dwn_span">Book an Appointment</span>
          </a>
        </div>
      </div>
      <div class="footer_div_3_inner_2">
        <span class="footer_div_3_spans_2">Follow Us</span>
        <div class="footer_div_3_bottom">
          <a href="https://www.facebook.com/MangroovyResidenceGouna/?ref=bookmarks" class="hrefs_links">
            <img class="idle_imgs" src="assets/img/fb_icon.png">
            <img class="active_imgs" src="assets/img/fb_icon_hover.png">
          </a>
          <a href="https://www.instagram.com/mangroovyresidenceelgouna/" class="hrefs_links">
            <img class="idle_imgs" src="assets/img/insta_icon.png">
            <img class="active_imgs" src="assets/img/insta_icon_hover.png">
          </a>
          <a href="https://www.linkedin.com/company/22313324/" class="hrefs_links">
            <img class="idle_imgs" src="assets/img/link_icon.png">
            <img class="active_imgs" src="assets/img/link_icon_hover.png">
          </a>
          <a href="https://www.youtube.com/channel/UCCJyjp14tpqMa0VouVxCIXw/" class="hrefs_links">
            <img class="idle_imgs" src="assets/img/yt_icon.png">
            <img class="active_imgs" src="assets/img/yt_icon_hover.png">
          </a>
        </div>
    </div>
  </div>
  <div class="col-sm-3 footer_divs footer_div_4">
      <div class="footer_div_4_inner">
        <span class="span_1">Subscribe to our Newsletter</span>
        <form class="news_form" formID="4">
          <input inputID="1" type="email" class="news_input form_inputs" placeholder="E-mail" required>
          <button class="news_btn" type="submit">Subscribe</button>
        </form>
      </div>
  </div>
  <div class="col-sm-3 footer_divs footer_div_1 footer_div_mobile_2">
    <div class="footer_div_1_inner">
      <span>© <?= date('Y'); ?> Mangroovy Residence</span>
      <span>All rights reserved</span>
    </div>
  </div>
  <!-- <div class="col-sm-3 footer_divs footer_div_4">
  </div> -->
</div>