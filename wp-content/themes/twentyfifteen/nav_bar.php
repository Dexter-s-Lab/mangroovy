<?php
global $post;
$curr_title = get_the_title();

$args = array( 'numberposts' =>-1, 'post_type'=>'extra_fields', 'suppress_filters' => 0, 'order' => 'ASC');
$broc = get_posts( $args );


foreach ( $broc as $post ) :   setup_postdata( $post );

$file = get_field('brochure', get_the_ID());

endforeach;
wp_reset_postdata();

// print_r($file);die;

?>
<div class="overlay" id="loading">
  <img src="assets/img/spinner.gif">
</div>

<div class="overlay" id="loading_2">
  <img src="assets/img/spinner.gif">
</div>
  <div class="nav_div container">
    <div class="col-sm-2 nav_divs nav_left_div">
      <a href="<?php echo get_site_url(); ?>">
        <img class="event_logo" src="assets/img/man_logo_header.png">
      </a>
    </div>
    <div class="col-sm-10 nav_divs nav_right_div">
      <div class="top_nav_right">
        <!-- test -->
      </div>
      <div class="bot_nav_right">

        <?php 

            foreach ($menu as $key => $value) {

              $class = "";

              if($value['title'] == $curr_title)
              $class = "active";

            // print_r($value['title']);die;

              ?>
              <a href="<?= $value['url'];?>">
                <span class="<?= $class;?> custom_<?= $value['title'];?> custom_<?= $value['slug'];?>"><?= $value['title'];?></span>
              </a>
              <?php
            }
            ?>
      </div>
    </div>
  </div>

<nav class="navbar navbar-default nav_mobile">
        <div class="container-fluid">
          <div class="navbar-header">
            <div class="nav_btn_div">
              <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
              </button>
            </div>
            <a class="navbar-brand" href="<?php echo get_site_url(); ?>">
              <img class="event_logo" src="assets/img/man_logo_header.png">
            </a>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">

              <?php 

            foreach ($menu as $key => $value) {

              $class = "";

              if($value['title'] == $curr_title)
              $class = "active";

            // print_r($value['title']);die;

              ?>

              <li class="<?= $class;?>">
                <a href="<?= $value['url'];?>">
                  <?= $value['title'];?>
                </a>
              </li>
              <?php
            }
            ?>
            </ul>
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>
<div class="fake_div">
</div>

  