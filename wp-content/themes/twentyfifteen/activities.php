
<?php

/*
 Template Name: Activites
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/activities.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">
<link href="assets/css/main.css" rel="stylesheet" media="screen">




<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;

$page_title = $post->post_title;
$gallery = $post->post_content;
// print_r($gallery);die;
$desc = get_field('desc', get_the_ID());

$array_menu = wp_get_nav_menu_items("main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}

global $post;
$page_id =  $post->ID;

// print_r($cat_array);die;
$args = array( 'numberposts' =>-1, 'post_type'=>'activity', 'suppress_filters' => 0, 'order' => 'ASC');
$act = get_posts( $args );
$act_items = array();


foreach ( $act as $post ) :   setup_postdata( $post );

$act_id = get_the_ID();
$title = $post->post_title;
$act_desc = get_field('desc', get_the_ID());
$thumb = wp_get_attachment_url( get_post_thumbnail_id($act_id) );
$color = get_field('color', get_the_ID());
$width = get_field('width', get_the_ID());
$height = get_field('height', get_the_ID());
$display = get_field('display', get_the_ID());
// print_r($thumb);

$act_items[] = array('title' => $title, 'thumb' => $thumb,  'color' => $color,  'width' => $width,
   'height' => $height, 'desc' => $act_desc, 'display' => $display);

endforeach;
wp_reset_postdata();

// print_r($act_items);die;
?>


<div class="main_div container">

  <?php 
  include 'nav_bar.php';
  ?>
  <div class="top_div">
    <span class="page_title"><?= $page_title; ?></span>
    <p class="page_desc"><?= $desc; ?></p>
  </div>
</div>
<div class="loc_div">
  <div role="main">

      <ul class="tiles-wrap animated" id="wookmark1">
        <?php
          foreach ($act_items as $key => $value) {
            ?>

            <li>
              <img src="<?= $value['thumb'] ?>" width="<?= $value['width'] ?>" height="<?= $value['height'] ?>">
              <?php
              // if($value['display'] != -1)
              {

              ?>
              <div class="inner_overlay" style="background-color:<?= $value['color'] ?>">
                <div class="inner_overlay_holder">
                  <span>
                    <?= $value['title'] ?>
                  </span>
                  <p>
                    <?= $value['desc'] ?>
                  </p>
                </div>
              </div>
              <?php } ?>
            </li>

            <?php
          }
          ?>
      </ul>
    </div>
</div>

<?php 
  include 'footer_div.php';
  ?>

<?php get_footer(); ?>

<script>

$( document ).ready(function() {


// $('#wookmark1').wookmark();

var wookmark1 = new Wookmark('#wookmark1', {
          outerOffset: 0,
          autoResize: true,
          offset: 5
      });

  $('.bot_nav_right span').on('click',function (e) {

  });

  

  });

$(window).scroll(function() {

//   var scroll = $(window).scrollTop();
//   var slide_height = parseInt($('.slider_div').css('height'));

//   if(scroll > 40)
//   {

//     $('.arrow-up-right').css('border-top-width', '0');
//     $('.arrow-up-left').css('border-top-width', '0');


//     $('.nav_bar').addClass('nav_bar_slide');

//   }

//   else
//   {

//     $('.arrow-up-right').css('border-top-width', '90px');
//     $('.arrow-up-left').css('border-top-width', '90px');

//     $('.nav_bar').removeClass('nav_bar_slide');
//   } 
// });


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';

 var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;


$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);

$('#loading ').fadeOut();

}

$(window).on('resize', function()
{

var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;

$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>