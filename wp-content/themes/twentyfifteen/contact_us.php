
<?php

/*
 Template Name: Contact us
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/contact_us.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 


$new_url = do_shortcode('[urlparam param="source"/]');
if(empty($new_url))
$new_url = '';

global $post;
$page_id =  $post->ID;

$title = $post->post_title;
$desc = get_field('desc', get_the_ID());
$call_us = get_field('call_us', get_the_ID());
// print_r($post);die;

$array_menu = wp_get_nav_menu_items("main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}



$args = array( 'numberposts' =>-1, 'post_type'=>'form_units', 'suppress_filters' => 0, 'order' => 'ASC');
$form_units = get_posts( $args );
$form_units_items = array();


foreach ( $form_units as $post ) :   setup_postdata( $post );

$title = $post->post_title;


$form_units_items[] = $title;

endforeach;
wp_reset_postdata();

// print_r($form_units_items);die;

?>


<div class="main_div container">

  <?php 
  include 'nav_bar.php';
  ?>
  <!-- <div class="nav_bar">
    <div id="navbar" class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
        <li class="logo_nav"><a href="<?php echo get_site_url(); ?>"><img class="event_logo" src="assets/img/man_logo_header.png"></a></li>
        <li class="menu_items <?= $class;?>">
          <a href="#home">Home</a>
        </li>
      </ul>
    </div>
  </div> -->
  <div class="top_div">
    <span><?= $page_title; ?></span>
    <p><?= $desc; ?></p>
  </div>
</div>
<div class="reg_div">
  <div class="reg_div_inner">
    <span>Register Here</span>
    <form class="reg_form" formID="1">
      <div class="form-group">
        <input inputID="1" type="text" class="form-control form_inputs" placeholder="NAME" required>
      </div>
      <div class="form-group">
        <input inputID="2" type="tex" class="form-control form_inputs" placeholder="MOBILE PHONE" required>
      </div>
      <div class="form-group">
        <input inputID="3" type="email" class="form-control form_inputs" placeholder="EMAIL" required>
      </div>
       <div class="form-group">
        <select inputID="4" class="form-control form_inputs pri_select" required>
          <option value="">
            UNIT
          </option>
          <?php
          foreach ($form_units_items as $key => $value) {
            ?>

            <option value="<?= $value; ?>">
              <?= $value; ?>
            </option>

            <?php
          }
          ?>
        </select>
      </div>
      <div class="form-group">
        <input inputID="5" type="tex" class="form-control form_inputs" placeholder="COUNTRY" required>
      </div>
      <input inputID="6" type="hidden" value="<?php echo $new_url;?>" name="source" class="form_inputs">
      <button type="submit" class="btn btn-default submit_btn">REGISTER</button>
    </form>
  </div>
</div>

<input type="hidden" class="thank_you_url" value="<?php echo get_permalink( 368 ); ?>">

<?php 
  include 'footer_div.php';
  ?>

<?php get_footer(); ?>


<script>

$( document ).ready(function() {


  $('.bot_nav_right span').on('click',function (e) {

  });

  $( ".news_form" ).submit(function( event ) {
    
    event.preventDefault();
    return false;

    $('#loading_2').show();

    var formID = $(this).attr('formID');
    var inputs = $(this).find('.form_inputs');

    $.each( inputs, function( key, value ) {
      
      inputID = $(value).attr('inputID');
      var input_old = $(value).val();
      $('#input_' + formID + '_' + inputID).val(input_old);


    });

    $('#gform_' + formID).submit();
    
    jQuery(document).bind('gform_post_render', function(){

      $('#loading_2').fadeOut();
      // return false;

      $.each( inputs, function( key, value ) {
        
        $(value).val('');

      });

    });

  });

});

$(window).scroll(function() {

//   var scroll = $(window).scrollTop();
//   var slide_height = parseInt($('.slider_div').css('height'));

//   if(scroll > 40)
//   {

//     $('.arrow-up-right').css('border-top-width', '0');
//     $('.arrow-up-left').css('border-top-width', '0');


//     $('.nav_bar').addClass('nav_bar_slide');

//   }

//   else
//   {

//     $('.arrow-up-right').css('border-top-width', '90px');
//     $('.arrow-up-left').css('border-top-width', '90px');

//     $('.nav_bar').removeClass('nav_bar_slide');
//   } 
// });


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';

 var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;


$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);

$('#loading ').fadeOut();

}

$(window).on('resize', function()
{

var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;

$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>