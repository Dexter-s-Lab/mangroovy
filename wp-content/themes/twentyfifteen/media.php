
<?php

/*
 Template Name: Media
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<link href="assets/css/bootstrap.css" rel="stylesheet" media="screen">
<link href="assets/css/media.css" rel="stylesheet" media="screen">
<link href="assets/css/style.css" rel="stylesheet" media="screen">


<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;

$main_bg = get_field('main_bg', get_the_ID());
$main_title = get_field('main_title', get_the_ID());
$main_text = get_field('main_text', get_the_ID());

$array_menu = wp_get_nav_menu_items("main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}

global $post;
$page_id =  $post->ID;

// print_r($cat_array);die;
$args = array( 'numberposts' =>-1, 'post_type'=>'events', 'suppress_filters' => 0, 'order' => 'ASC');
$events = get_posts( $args );
$events_items = array();


foreach ( $events as $post ) :   setup_postdata( $post );

$title = $post->post_title;
$desc = get_field('desc', get_the_ID());
$thumb = wp_get_attachment_url( get_post_thumbnail_id(get_the_ID()) );
$post_slug = $post->post_name;
$link = get_site_url().'/events/'.$post_slug;


// $all_images = array();
// $gallery = get_field('gallery', get_the_ID());
// $gallery = $gallery[0]['ngg_id'];
// global $nggdb;
// $images = $nggdb -> get_gallery($gallery);
// foreach ( $images as $image ) {
//  $all_images[] = $image->imageURL;
// }


$events_items[] = array('title' => $title, 'desc' => $desc, 'thumb' => $thumb, 'link' => $link);

endforeach;
wp_reset_postdata();


// print_r($events_items);die;
?>


<div class="main_div container">

  <?php 
  include 'nav_bar.php';
  ?>
  <div class="top_div" style="background-image:url(<?= $main_bg; ?>)">
    <div class="top_text_div">
      <!-- <span class="top_span_1">
        <?= $page_title; ?>
      </span> -->
    </div>
  </div>
</div>
<div class="fac_div">
  <div id="owl-demo" class="owl-carousel owl-theme owl-demo">
          <?php 
          foreach ($events_items as $key => $value) {
            ?>
            <a href="<?= $value['link'];?>">
              <div class="item client_owl">
                <div class="owl_overlay"></div>
                <img src="<?= $value['thumb'];?>">
                <span><?= $value['title'];?></span>
              </div>
            </a>
            <?php
          }
          ?>
        </div>
</div>

<?php 
  include 'footer_div.php';
  ?>

<?php get_footer(); ?>

<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<script type="text/javascript" src="assets/owl_carousel_assets/owl.carousel.js"></script>

<script>

$( document ).ready(function() {


  $('.bot_nav_right span').on('click',function (e) {

  });

  });

$(window).scroll(function() {

//   var scroll = $(window).scrollTop();
//   var slide_height = parseInt($('.slider_div').css('height'));

//   if(scroll > 40)
//   {

//     $('.arrow-up-right').css('border-top-width', '0');
//     $('.arrow-up-left').css('border-top-width', '0');


//     $('.nav_bar').addClass('nav_bar_slide');

//   }

//   else
//   {

//     $('.arrow-up-right').css('border-top-width', '90px');
//     $('.arrow-up-left').css('border-top-width', '90px');

//     $('.nav_bar').removeClass('nav_bar_slide');
//   } 
// });


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';

 var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;


$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);

var owl = $(".owl-demo");
 
  owl.owlCarousel({
     
     // items : 5,
      items :2, //10 items above 1000px browser width
      itemsDesktop : [1000,2], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,1], // betweem 900px and 601px
      itemsTablet: [600,1], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
      navigation : true,
      pagination: false,
      touchDrag: true,
      autoplay: true,
      autoplayHoverPause: false,
      navigationText: [
        "<i class='icon-chevron-left icon-white'><</i>",
        "<i class='icon-chevron-right icon-white'>></i>"
      ]
 
  });


$('#loading ').fadeOut();

}

$(window).on('resize', function()
{

var windowWidth = $(window).width();

if(windowWidth > 768)
{
  var loc_height = $('.loc_left_div').css('height');
  $('.loc_right_div').css('height',loc_height);
}

var footer_height = $('.footer_div_1').css('height');
var footer_height_2 = $('.footer_div_2').css('height');
var footer_height_3 = $('.footer_div_3').css('height');

if(parseInt(footer_height_2) > parseInt(footer_height))
footer_height = footer_height_2;

if(parseInt(footer_height_3) > parseInt(footer_height))
footer_height = footer_height_3;

$('.footer_div_1').css('height',footer_height);
$('.footer_div_2').css('height',footer_height);
$('.footer_div_3').css('height',footer_height);
$('.footer_div_4').css('height',footer_height);


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>