<nav class="navbar navbar-default navbar-fixed-top mobile_nav">
        <div class="container-fluid">
          <div class="navbar-header">
            <a class="navbar-brand mobile_links" href="<?= home_url().'/#home'; ?>" >
              <img src="assets/img/marassi_logo.png">
            </a>
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <?php
                foreach ($nav_items as $key => $value) {
                  ?>
                  <li><a href="<?= home_url().'/#'.$value['link']; ?>" class="mobile_links"><?= $value['title']; ?></a></li>
                  <?php
                }
                ?>
            </ul>
          </div>
        </div>
      </nav>