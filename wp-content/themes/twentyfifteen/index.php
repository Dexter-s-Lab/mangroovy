
<?php

/*
 Template Name: Homepage OLD
 */

$page_title = $wp_query->post->post_title;

?>
<title><?= $page_title; ?></title>

<base href="<?php bloginfo('template_directory');?>/twentyfifteen"/>
<!-- <link href="assets/css/bootstrap.css" rel="stylesheet" media="screen"> -->
<script type="text/javascript" src="assets/js/jquery-2.1.4.min.js"></script>
<link href="assets/owl_carousel_assets/owl.carousel.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.theme.css" rel="stylesheet" media="screen">
<link href="assets/owl_carousel_assets/owl.transitions.css" rel="stylesheet" media="screen">
<link href="style_home.css" rel="stylesheet" media="screen">
<link rel="stylesheet" href="assets/css/responsive-nav.css">
<link href="assets/css/bootstrap_3.3.7.min.css" rel="stylesheet" media="screen">
<script src="assets/js/responsive-nav.js"></script>

<?php 
//include 'spinner.php'; 
?>

<?php
get_header(); 

global $post;
$page_id =  $post->ID;

$portfolio = get_field('portfolio', get_the_ID());

$who_we_are = get_field('who_we_are', get_the_ID());
$who_we_are = strip_tags($who_we_are, '<p>');
$who_we_are = strip_tags($who_we_are, '<span>');

// print_r($who_we_are_right);die;

$args = array( 'numberposts' =>-1, 'post_type'=>'testimonials', 'suppress_filters' => 0, 'order' => 'ASC');
$navs = get_posts( $args );
$test_items = array();

foreach ( $navs as $post ) :   setup_postdata( $post );

$author = get_field('author', get_the_ID());
$title = get_field('title', get_the_ID());
$testimonial = get_field('testimonial', get_the_ID());

$test_items[] = array('author'=> $author, 'title'=> $title, 'testimonial'=> $testimonial);
// print_r($test_items);die;

endforeach;
wp_reset_postdata();

$array_menu = wp_get_nav_menu_items("Main");
$menu = array();
$counter = 0;
foreach ($array_menu as $m) {

    if (empty($m->menu_item_parent)) {
        $menu[$counter] = array();
        $menu[$counter]['title']       =   $m->title;
        $menu[$counter]['url']         =   $m->url;
    }

    $counter++;
}
// print_r($menu);


?>


<link href="assets/css/home.css" rel="stylesheet" media="screen">

<?php 
//include 'analyticstracking.php' 
?>
<?php 
//include 'mobile_nav.php' 
?>

<div class="main_div container">
  <div class="nav_bar_empty"></div>
  <?php 
  include 'nav_bar.php';
  ?>
  <div class="main_div_2">
    <div class="arrow-up-right"></div>
    <div class="arrow-up-left"></div>
    
    <div class="client_list_div">
      <div class="title_div_1">
        <span class="title_div_1_span_1">this is</span>
        <span class="title_div_1_span_2">Who We Are</span>
      </div>
      <div class="who_we_are_div container">
        <p><?= $who_we_are; ?></p>
      </div>
      <div class="title_div_2">
        <span class="title_div_2_span_1">What</span>
        <span class="title_div_2_span_2">They Said</span>
      </div>
      <div class="test_div">
        <p>
          "<?= $test_items[0]['testimonial']; ?>"
        </p>
        <div class="test_div_bot">
          <span>
            - <?= $test_items[0]['author']; ?>
          </span>
          <span>
            <?= $test_items[0]['title']; ?>
          </span>
        </div>
      </div>
    </div>
    <div class="arrow-up-right_footer"></div>
    <div class="arrow-up-left_footer"></div>
  </div>
  <div class="footer">
    <div class="footer_div_1">
      <span>follow us on..</span>
      <div class="footer_img_div">
        <img src="assets/img/yt_icon.png">
        <img src="assets/img/fb_icon.png">
        <img src="assets/img/insta_icon.png">
      </div>
    </div>
    <div class="footer_copy_div">
      <span>&copy; 2017 EVENT HOUSE. All Rights Reserved. Developed & Designed by Hug Digital</span>
    </div>
  </div>
</div>
<?php get_footer(); ?>
<script>

$( document ).ready(function() {

  $('.item').on('click',function (e) {
      
      var itemCount = $(this).attr('itemCount');

      $('.active_cont').fadeOut('');
      $('.active_cont').removeClass('active_cont');
      
      $('.item_cont_' + itemCount).fadeIn('');
      $('.item_cont_' + itemCount).addClass('active_cont');

      
      $('.active_item').removeClass('active_item');
      $('.item_' + itemCount).addClass('active_item');


    });

$(window).scroll(function() {

  var scroll = $(window).scrollTop();
  var slide_height = parseInt($('.slider_div').css('height'));

  if(scroll > 40)
  {

    $('.arrow-up-right').css('border-top-width', '0');
    $('.arrow-up-left').css('border-top-width', '0');


    $('.nav_bar').addClass('nav_bar_slide');

  }

  else
  {

    $('.arrow-up-right').css('border-top-width', '90px');
    $('.arrow-up-left').css('border-top-width', '90px');

    $('.nav_bar').removeClass('nav_bar_slide');
  } 
});


});

window.onload = function() {

 var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.nav_bar_empty').css('height', $('.nav_bar').css('height'));

 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);
 $('.arrow-up-right').css('border-top-width', '90px');
 $('.arrow-up-left').css('border-top-width', '90px');


 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);
 $('.arrow-up-right_footer').css('border-bottom-width', '90px');
 $('.arrow-up-left_footer').css('border-bottom-width', '90px');

}

$(window).on('resize', function()
{

  var main_width = parseInt($('.main_div_2').css('width'));
 main_width = Math.round(main_width / 2);
 main_width = main_width + 'px';
 
 $('.arrow-up-right').css('border-right-width', main_width);
 $('.arrow-up-left').css('border-left-width', main_width);

 $('.arrow-up-right_footer').css('border-left-width', main_width);
 $('.arrow-up-left_footer').css('border-right-width', main_width);


});

</script>

<style type="text/css">
html
{
  margin-top: 0 !important;
}
</style>